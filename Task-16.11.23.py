def check1_pow_2(number):               # Version 1
    if(number & (-number)==number):
        return True
    else:
        return False
number1=16384   
print(f"The {number1} is a power of 2:",check1_pow_2(number1)) 

def check_pw_2(number):                 # Version 2
    if (number& (number-1)==0 and number!=0): 
        return True
    else:
        return False
number2=1
print(f"The {number2} is a power of 2:", check_pw_2(number2))   

def check3_pow_2(number):                # Version 3
    ls=[]
    while(number!=0):
        rem=number%2
        ls.append(rem)
        number//=2
    if ls.count(1)==1:
        return True
    else:
        return False
number3=15
print(f"The {number3} is a power of 2:",check3_pow_2(number3)) 

def check4_pow_2(number4):       # Version 4
    bin_num=bin(number4)
    str_bin_num=str(bin_num)
    if str_bin_num.count("1")==1:
        return True
    else:
        return False
number4=171
print(f"The {number4} is a power of two: ",check4_pow_2(number4))

